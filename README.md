# Pokemon UPC Version
Trabajo final de Programación II - UPC - ciclo 2015 - 01.

Carpeta Del Proyecto: https://1drv.ms/f/s!AlBOXn97PZ--xVe2LfvtRMbeFmkQ

Descripciòn:
  - Este juego fue escrito en c++, con el uso de Widows Forms.
  - Se utilizaron tilesets para la creación del mapa, el cual se basa en la plazuela de la UPC San Isidro, y se utilizo Photoshop para la edición del mismo y de los gimnasios.
  - Se utilizaron las técnicas de herencia múltiple con constructores implicitos y de sobrecarga para la creación de una gran variedad de Pokemons y diferentes tipos.
  - El juego tiene 30 pokemons en total.
  - Se usó polimorfismo y virtualización para darles atributos únicos a cada pokemon.
  - Se utilizaron sprites para la movilización del jugador y para el movimiento de ciertos pokemons.
  - La movilidad del jugador a diferencia de la rubrica es estática(No se mueve del centro de la pantalla).
  - Nos basamos lo más que pudimos en el juego original, para conservar la historia, pero a la vez cumplir con la rúbrica del trabajo.
  - El juego cuenta con muchas animaciones únicas que tienen la finalidad de imitar al juego original.
  - El juego cuenta con 3 gimnasios, los cuales otorgan 3 respectivas medallas.
  - Los mapas del juego mantienen una estructura relacionada con arreglos de coordenadas, donde las colisiones a todo tipo de objetos y terrenos estan implementados.
  - El juego cuenta con un centro pokemon, donde se pueden curar a los pokemones y donde aparecerás si te quedas sin pokemones.
  - El juego está completamente programado para implementar nuevos lugares, puertas, objetos secretos, etc.
  - El juego permite un maximo de 6 pokemones por jugador.
  - El juego tiene una pequeña base de datos con muchos ataques por cada tipo, con respectivos daños y efectos secundarios.
  - El juego tiene implementado una funcion para crear interfaces de historia de una manera rápida, el juego esta repleto de ellas.
  - Dentro del juego solo un pokemón puede evolucionar, pero no es mediante niveles.
  -  
  
Integrantes:
- Diego Jara
- Oscar Medina


Profesor:
- Christian Nagayoshi
